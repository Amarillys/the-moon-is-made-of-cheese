// TO COMPILE : gcc -o game src/game.c -lSDL2 -lSDL2_ttf -lSDL2_image

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SDL_BUTTON_LEFT 1
#define nbTrap 10

// structures
struct item {
  int x;
  int y;
  int sprite; // 0 no, 1 yes
  int exist;  // cheese/trap : if cheese was eaten or not (1 no, 0 yes)
              // mouse : if SDL_KEYDOWN/SDL_MOUSEBUTTONDOWN (0 no, 1 yes)
};

struct count {
  int i;       // int form
  char c[999]; // char form
};

struct time {
  int current;
  int previous;
};

struct status {
  int current; // for layout, 0 nothing, 1 title, 2 game, 3 results, 4 help
};
// end structures

// prototypes
int init(SDL_Window **window, SDL_Renderer **renderer, SDL_Surface *surface,
         int w, int h);
SDL_Texture *loadPictureBitmap(const char path[], SDL_Renderer *renderer);
SDL_Texture *loadPictureOther(const char path[], SDL_Renderer *renderer);
int loadLayout(SDL_Texture *layout, SDL_Renderer *renderer);
int loadTitle(TTF_Font *fontSmall, TTF_Font *fontLarge, SDL_Color color,
              SDL_Renderer *renderer);
int loadHelp(TTF_Font *fontSmall, TTF_Font *fontLarge, SDL_Color color,
             SDL_Renderer *renderer);
int loadDifficulty(SDL_Renderer *renderer, SDL_Color color, TTF_Font *font);
int loadMouse(SDL_Texture *mousePicture, struct item *mouse,
              SDL_Renderer *renderer);
int loadCheese(SDL_Texture *cheesePicture, struct item *cheese,
               struct item *mouse, SDL_Renderer *renderer);
int loadTrap(SDL_Texture *trapPicture, struct item *trap, struct item *mouse,
             struct item *cheese, SDL_Renderer *renderer);
void cheesePos(struct item *cheese, struct item *mouse);
int mousePos(struct item *mouse, struct item *cheese, struct item *trap,
             SDL_Event event, SDL_Renderer *renderer);
int pointsCount(struct count *points, SDL_Renderer *renderer,
                struct item *mouse, struct item *cheese, struct item *trap,
                SDL_Color color, TTF_Font *font);
int timeCount(struct time *clockTime, struct count *timer, struct item *mouse,
              struct item *cheese, struct item *trap, struct status *layout,
              SDL_Renderer *renderer, SDL_Color color, TTF_Font *font,
              SDL_Texture *gameLayout);
int printResults(struct count *points, SDL_Renderer *renderer, SDL_Color color,
                 TTF_Font *font);
int printText(const char textToPrint[], SDL_Renderer *renderer, SDL_Rect dst,
              TTF_Font *font, SDL_Color color);
int helpButton(int x, int y);
int playButton(int x, int y);
int pauseButton(int x, int y);
int pauseButton2(int x, int y);
int pauseButton3(int x, int y);
int restartButton(int x, int y);
int easyButton(int x, int y);
int hardButton(int x, int y);
// end prototypes

// main
int main(int argc, char const *argv[]) {

  // init
  int status = EXIT_FAILURE;

  // font
  if (TTF_Init() == -1) {
    fprintf(stderr, "Error TTF_Init : %s\n", TTF_GetError());
    goto Quit;
  }
  TTF_Font *fontSmall = TTF_OpenFont("font/caveat.ttf", 30);
  TTF_Font *fontMedium = TTF_OpenFont("font/caveat.ttf", 50);
  TTF_Font *fontLarge = TTF_OpenFont("font/caveat.ttf", 80);
  SDL_Color darkGrey = {20, 20, 20};

  // window and renderer
  SDL_Window *window = NULL;
  SDL_Renderer *renderer = NULL;
  SDL_Surface *surface = NULL;
  if (0 != init(&window, &renderer, surface, 640, 480))
    goto Quit;

  // title, game, difficulty, results layout
  SDL_Texture *titleLayout = loadPictureBitmap("picture/title.bmp", renderer);
  SDL_Texture *gameLayout = loadPictureBitmap("picture/game.bmp", renderer);
  SDL_Texture *resultsLayout =
      loadPictureBitmap("picture/results.bmp", renderer);
  SDL_Texture *helpLayout = loadPictureBitmap("picture/help.bmp", renderer);
  SDL_Texture *difficultyLayout =
      loadPictureBitmap("picture/difficulty.bmp", renderer);
  struct status layout = {
      0}; // 0 nothing, 1 title, 2 game, 3 results, 4 help, 5 difficulty

  // mouse, cheese, trap
  struct item mouse = {0, 0, 0, 0};
  SDL_Texture *mousePicture = loadPictureOther("picture/mouse.png", renderer);
  struct item cheese = {0, 0, 0, 0};
  SDL_Texture *cheesePicture = loadPictureOther("picture/cheese.png", renderer);
  struct item *trap = (struct item *)malloc(nbTrap * sizeof(struct item));
  for (int i = 0; i < nbTrap; i++) {
    trap[i].x = 0;
    trap[i].y = 0;
    trap[i].sprite = 0;
    trap[i].exist = 0;
  }
  SDL_Texture *trapPicture = loadPictureOther("picture/trap.png", renderer);

  // others
  SDL_Event event;
  struct count points = {0, "0"}; // points
  struct count timer = {0, "0"};  // timer
  struct time clockTime = {0, 0};
  int x, y;          // position
  srand(time(NULL)); // time
  // end init

  // main loop
  while (1) {

  Title:

    // until you close the window
    if (SDL_PollEvent(&event)) {
      if (event.type == SDL_QUIT)
        goto Quit;
      if (event.type == SDL_MOUSEBUTTONUP)
        mouse.exist = 0;
    }

    // print the title layout if not the current layout
    if (layout.current != 1) {
      if (loadLayout(titleLayout, renderer) == -1)
        goto Quit;
      layout.current = 1;
      if (loadTitle(fontSmall, fontLarge, darkGrey, renderer) == -1)
        goto Quit;
    }

    // enter game ?
    SDL_PumpEvents();
    SDL_GetMouseState(&x, &y);

    if (helpButton(x, y) &&
        SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT) &&
        mouse.exist == 0) {
      if (layout.current != 4) {
        if (loadLayout(helpLayout, renderer) == -1)
          goto Quit;
        layout.current = 4;
        if (loadHelp(fontSmall, fontLarge, darkGrey, renderer) == -1)
          goto Quit;
      }
      while (1) {

        // until you close the window
        if (SDL_PollEvent(&event) && event.type == SDL_QUIT)
          goto Quit;

        // or press the pause button
        SDL_PumpEvents();
        SDL_GetMouseState(&x, &y);
        if (pauseButton3(x, y) &&
            SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT))
          goto Title;

        SDL_Delay(2);
      }
    }

    if (playButton(x, y) &&
        SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT)) {

      SDL_RenderClear(renderer);

      while (1) {

        SDL_PumpEvents();
        SDL_GetMouseState(&x, &y);
        if (pauseButton3(x, y) &&
            SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT))
          goto Title;
        if (SDL_PollEvent(&event) && event.type == SDL_QUIT)
          goto Quit;

        // print the difficulty layout if not the current layout
        if (layout.current != 5) {
          if (loadLayout(difficultyLayout, renderer) == -1)
            goto Quit;
          layout.current = 5;
          if (loadDifficulty(renderer, darkGrey, fontMedium) == -1)
            goto Quit;
          SDL_RenderPresent(renderer);
        }

        if (easyButton(x, y) &&
            SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT)) {

        GameEasy:

          mouse.x = 305;
          mouse.y = 225;
          cheese.exist = 0;
          points.i = 0;
          timer.i = 60;

          while (1) {

            if (SDL_PollEvent(&event))
              // until you close the window
              if (event.type == SDL_QUIT)
                goto Quit;
            // don't allow to continue pressing a key
            if (mouse.exist == 1 && event.type == SDL_KEYUP)
              mouse.exist = 0;

            // return to title
            SDL_PumpEvents();
            SDL_GetMouseState(&x, &y);
            if (pauseButton(x, y) &&
                SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT))
              goto Title;

            // timer
            if (timeCount(&clockTime, &timer, &mouse, &cheese, trap, &layout,
                          renderer, darkGrey, fontSmall, gameLayout) == -1)
              goto Quit;

            if (timer.i == 0)
              goto ResultsEasy;

            // game layout
            if (layout.current != 2) {
              if (loadLayout(gameLayout, renderer) == -1)
                goto Quit;
              layout.current = 2;
            }

            // points
            if (pointsCount(&points, renderer, &mouse, &cheese, trap, darkGrey,
                            fontSmall) == -1)
              goto Quit;

            // mouse
            if (loadMouse(mousePicture, &mouse, renderer) == -1)
              goto Quit;

            // cheese
            if (loadCheese(cheesePicture, &cheese, &mouse, renderer) == -1)
              goto Quit;

            // new mouse position
            if (mousePos(&mouse, &cheese, trap, event, renderer) == 0)
              layout.current = 0;

            // print all
            SDL_RenderPresent(renderer);

            SDL_Delay(2);
          }

        ResultsEasy:

          if (timer.i == 0) {
            if (loadLayout(resultsLayout, renderer) == -1)
              goto Quit;
            layout.current = 3;
            if (printResults(&points, renderer, darkGrey, fontLarge) == -1)
              goto Quit;
            SDL_RenderPresent(renderer);

            while (1) {
              SDL_PumpEvents();
              // until you close the window
              if (SDL_PollEvent(&event) && event.type == SDL_QUIT)
                goto Quit;
              // return to title
              SDL_GetMouseState(&x, &y);
              if (pauseButton2(x, y) &&
                  SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT)) {
                mouse.exist = 1;
                goto Title;
              }
              // restart a game
              if (restartButton(x, y) &&
                  SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT))
                goto GameEasy;
            }
          }
          SDL_Delay(2);
        }

        if (hardButton(x, y) &&
            SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT)) {

        GameHard:

          mouse.x = 305;
          mouse.y = 225;
          cheese.exist = 0;
          points.i = 0;
          timer.i = 60;
          for (int i = 0; i < nbTrap; i++)
            trap[i].exist = 0;

          while (1) {

            if (SDL_PollEvent(&event))
              // until you close the window
              if (event.type == SDL_QUIT)
                goto Quit;
            // don't allow to continue pressing a key
            if (mouse.exist == 1 && event.type == SDL_KEYUP)
              mouse.exist = 0;

            // return to title
            SDL_PumpEvents();
            SDL_GetMouseState(&x, &y);
            if (pauseButton(x, y) &&
                SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT))
              goto Title;

            // timer
            if (timeCount(&clockTime, &timer, &mouse, &cheese, trap, &layout,
                          renderer, darkGrey, fontSmall, gameLayout) == -1)
              goto Quit;
            for (int i = 0; i < nbTrap; i++) {
              if (timer.i == 0 ||
                  (trap[i].x == mouse.x && trap[i].y == mouse.y))
                goto ResultsHard;
            }

            // game layout
            if (layout.current != 2) {
              if (loadLayout(gameLayout, renderer) == -1)
                goto Quit;
              layout.current = 2;
            }

            // points
            if (pointsCount(&points, renderer, &mouse, &cheese, trap, darkGrey,
                            fontSmall) == -1)
              goto Quit;

            // mouse
            if (loadMouse(mousePicture, &mouse, renderer) == -1)
              goto Quit;

            // cheese
            if (loadCheese(cheesePicture, &cheese, &mouse, renderer) == -1)
              goto Quit;

            // trap
            if (loadTrap(trapPicture, trap, &mouse, &cheese, renderer) == -1)
              goto Quit;

            // new mouse position
            if (mousePos(&mouse, &cheese, trap, event, renderer) == 0)
              layout.current = 0;

            // print all
            SDL_RenderPresent(renderer);

            SDL_Delay(2);
          }

        ResultsHard:

          for (int i = 0; i < nbTrap; i++) {
            if (timer.i == 0 ||
                (trap[i].x == mouse.x && trap[i].y == mouse.y)) {
              if (loadLayout(resultsLayout, renderer) == -1)
                goto Quit;
              layout.current = 3;
              if (printResults(&points, renderer, darkGrey, fontLarge) == -1)
                goto Quit;
              SDL_RenderPresent(renderer);

              while (1) {
                SDL_PumpEvents();
                // until you close the window
                if (SDL_PollEvent(&event) && event.type == SDL_QUIT)
                  goto Quit;
                // return to title
                SDL_GetMouseState(&x, &y);
                if (pauseButton2(x, y) && SDL_GetMouseState(NULL, NULL) &
                                              SDL_BUTTON(SDL_BUTTON_LEFT)) {
                  mouse.exist = 1;
                  goto Title;
                }
                // restart a game
                if (restartButton(x, y) &&
                    SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT))
                  goto GameHard;
              }
            }
            SDL_Delay(2);
          }
          SDL_Delay(2);
        }
        SDL_Delay(2);
      }
      SDL_Delay(2);
    }
  }

  status = EXIT_SUCCESS;

Quit:
  free(trap);
  if (NULL != mousePicture)
    SDL_DestroyTexture(mousePicture);
  if (NULL != trapPicture)
    SDL_DestroyTexture(trapPicture);
  if (NULL != cheesePicture)
    SDL_DestroyTexture(cheesePicture);
  if (NULL != titleLayout)
    SDL_DestroyTexture(titleLayout);
  if (NULL != helpLayout)
    SDL_DestroyTexture(helpLayout);
  if (NULL != gameLayout)
    SDL_DestroyTexture(gameLayout);
  if (NULL != resultsLayout)
    SDL_DestroyTexture(resultsLayout);
  if (NULL != difficultyLayout)
    SDL_DestroyTexture(difficultyLayout);
  if (NULL != renderer)
    SDL_DestroyRenderer(renderer);
  if (NULL != window)
    SDL_DestroyWindow(window);
  TTF_CloseFont(fontSmall);
  TTF_CloseFont(fontLarge);
  TTF_Quit();
  SDL_Quit();
  return status;
}
// end main

// functions
int init(SDL_Window **window, SDL_Renderer **renderer, SDL_Surface *surface,
         int w, int h) {
  // init video
  if (0 != SDL_Init(SDL_INIT_VIDEO)) {
    fprintf(stderr, "Error SDL_Init : %s", SDL_GetError());
    return -1;
  }
  *window =
      SDL_CreateWindow("THE MOON IS MADE OF CHEESE", SDL_WINDOWPOS_CENTERED,
                       SDL_WINDOWPOS_CENTERED, 640, 480, SDL_WINDOW_SHOWN);
  if (NULL == window) {
    fprintf(stderr, "Error SDL_CreateWindow : %s", SDL_GetError());
    return -1;
  }
  *renderer = SDL_CreateRenderer(
      *window, -1, SDL_RENDERER_ACCELERATED || SDL_RENDERER_PRESENTVSYNC);
  if (NULL == renderer) {
    fprintf(stderr, "Error SDL_CreateRenderer : %s", SDL_GetError());
    return -1;
  }
  // init icon
  SDL_Surface *icon = NULL;
  icon = SDL_LoadBMP("picture/mouse.bmp");
  if (NULL == icon) {
    fprintf(stderr, "Error SDL_LoadBMP : %s", SDL_GetError());
    return -1;
  }
  SDL_SetWindowIcon(*window, icon);
  return 0;
}

int loadLayout(SDL_Texture *layout, SDL_Renderer *renderer) {
  if (NULL == layout)
    return -1;
  SDL_RenderCopy(renderer, layout, NULL, NULL);
  SDL_RenderPresent(renderer);
  return 0;
}

int loadTitle(TTF_Font *fontSmall, TTF_Font *fontLarge, SDL_Color color,
              SDL_Renderer *renderer) {
  SDL_Rect dst_corner = {575, 430, 0, 0};
  if (printText("v2.0", renderer, dst_corner, fontSmall, color) == -1)
    return -1;

  SDL_Rect dst_title1 = {130, 20, 0, 0};
  if (printText("The Moon Is", renderer, dst_title1, fontLarge, color) == -1)
    return -1;

  SDL_Rect dst_title2 = {90, 100, 0, 0};
  if (printText("Made Of Cheese", renderer, dst_title2, fontLarge, color) == -1)
    return -1;

  SDL_Rect dst_play = {222, 315, 0, 0};
  if (printText("PLAY", renderer, dst_play, fontLarge, color) == -1)
    return -1;

  SDL_RenderPresent(renderer);
  return 0;
}

int loadHelp(TTF_Font *fontSmall, TTF_Font *fontLarge, SDL_Color color,
             SDL_Renderer *renderer) {
  SDL_Rect dst_help = {240, 20, 0, 0};
  if (printText("Help", renderer, dst_help, fontLarge, color) == -1)
    return -1;

  SDL_Rect dst_game = {25, 132, 0, 0};
  if (printText("You will have 1 minute to eat as many cheeses as you can",
                renderer, dst_game, fontSmall, color) == -1)
    return -1;

  SDL_Rect dst_arrows = {250, 220, 0, 0};
  if (printText("Use the arrows on your keyboard", renderer, dst_arrows,
                fontSmall, color) == -1)
    return -1;

  SDL_Rect dst_arrows2 = {250, 250, 0, 0};
  if (printText("to move the mouse", renderer, dst_arrows2, fontSmall, color) ==
      -1)
    return -1;

  SDL_Rect dst_exit = {275, 410, 0, 0};
  if (printText("Press to return to title", renderer, dst_exit, fontSmall,
                color) == -1)
    return -1;

  SDL_RenderPresent(renderer);
  return 0;
}

int loadDifficulty(SDL_Renderer *renderer, SDL_Color color, TTF_Font *font) {
  SDL_Rect dst_easy = {260, 110, 0, 0};
  if (printText("EASY", renderer, dst_easy, font, color) == -1)
    return -1;
  SDL_Rect dst_hard = {255, 265, 0, 0};
  if (printText("HARD", renderer, dst_hard, font, color) == -1)
    return -1;
  return 0;
}

int loadMouse(SDL_Texture *mousePicture, struct item *mouse,
              SDL_Renderer *renderer) {
  if (mouse->sprite == 0) {
    if (NULL == mousePicture)
      return -1;
    SDL_Rect dst = {mouse->x, mouse->y, 0, 0}, src = {3, 3, 24, 23};
    SDL_QueryTexture(mousePicture, NULL, NULL, &dst.w, &dst.h);
    SDL_RenderCopy(renderer, mousePicture, &src, &dst);
    mouse->sprite = 1;
  }
  return 0;
}

int loadCheese(SDL_Texture *cheesePicture, struct item *cheese,
               struct item *mouse, SDL_Renderer *renderer) {
  if (cheese->sprite == 0) {
    if (NULL == cheesePicture)
      return -1;
    if (cheese->exist == 0) {
      do {
        cheese->x = 5 + 30 * (rand() % 21);
        cheese->y = 45 + 30 * (rand() % 14);
      } while (cheese->x == mouse->x && cheese->y == mouse->y &&
               (cheese->x - mouse->x > 2 || cheese->x - mouse->x < -2));
      cheese->exist = 1;
    }
    SDL_Rect dst = {cheese->x, cheese->y, 0, 0}, src = {4, 2, 21, 23};
    SDL_QueryTexture(cheesePicture, NULL, NULL, &dst.w, &dst.h);
    SDL_RenderCopy(renderer, cheesePicture, &src, &dst);
    cheese->sprite = 1;
  }
  return 0;
}

int loadTrap(SDL_Texture *trapPicture, struct item *trap, struct item *mouse,
             struct item *cheese, SDL_Renderer *renderer) {
  for (int i = 0; i < nbTrap; i++) {
    if (trap[i].sprite == 0) {
      if (NULL == trapPicture)
        return -1;
      if (trap[i].exist == 0) {
        do {
          trap[i].x = 5 + 30 * (rand() % 21);
          trap[i].y = 45 + 30 * (rand() % 14);
        } while ((trap[i].x == mouse->x && trap[i].y == mouse->y) ||
                 (trap[i].x == cheese->x && trap[i].y == cheese->y));
        trap[i].exist = 1;
      }
      SDL_Rect dst = {trap[i].x, trap[i].y, 0, 0}, src = {0, 0, 30, 27};
      SDL_QueryTexture(trapPicture, NULL, NULL, &dst.w, &dst.h);
      SDL_RenderCopy(renderer, trapPicture, &src, &dst);
      trap[i].sprite = 1;
    }
  }
  return 0;
}

SDL_Texture *loadPictureBitmap(const char path[], SDL_Renderer *renderer) {
  SDL_Surface *tmp = NULL;
  SDL_Texture *texture = NULL;
  tmp = SDL_LoadBMP(path);
  if (NULL == tmp) {
    fprintf(stderr, "Error SDL_LoadBMP : %s", SDL_GetError());
    return NULL;
  }
  texture = SDL_CreateTextureFromSurface(renderer, tmp);
  SDL_FreeSurface(tmp);
  if (NULL == texture) {
    fprintf(stderr, "Error SDL_CreateTextureFromSurface : %s", SDL_GetError());
    return NULL;
  }
  return texture;
}

SDL_Texture *loadPictureOther(const char path[], SDL_Renderer *renderer) {
  SDL_Texture *texture = NULL;
  SDL_Surface *tmp = IMG_Load(path);
  if (!tmp) {
    printf("Error IMG_Load : %s", SDL_GetError());
    return NULL;
  }
  texture = SDL_CreateTextureFromSurface(renderer, tmp);
  SDL_FreeSurface(tmp);
  if (NULL == texture) {
    fprintf(stderr, "Error SDL_CreateTextureFromSurface : %s", SDL_GetError());
    return NULL;
  }
  return texture;
}

int mousePos(struct item *mouse, struct item *cheese, struct item *trap,
             SDL_Event event, SDL_Renderer *renderer) {
  SDL_PumpEvents();
  if (SDL_GetKeyboardState(NULL)[SDL_SCANCODE_LEFT] && mouse->exist == 0) {
    if (mouse->x > 10) {
      mouse->x = mouse->x - 30;
      mouse->sprite = 0;
      cheese->sprite = 0;
      for (int i = 0; i < nbTrap; i++)
        trap[i].sprite = 0;
      mouse->exist = 1;
      SDL_RenderClear(renderer);
      SDL_Delay(75);
      return 0;
    }
  }
  if (SDL_GetKeyboardState(NULL)[SDL_SCANCODE_RIGHT] && mouse->exist == 0) {
    if (mouse->x < 580) {
      mouse->x = mouse->x + 30;
      mouse->sprite = 0;
      cheese->sprite = 0;
      for (int i = 0; i < nbTrap; i++)
        trap[i].sprite = 0;
      mouse->exist = 1;
      SDL_RenderClear(renderer);
      SDL_Delay(75);
      return 0;
    }
  }
  if (SDL_GetKeyboardState(NULL)[SDL_SCANCODE_UP] && mouse->exist == 0) {
    if (mouse->y > 45) {
      mouse->y = mouse->y - 30;
      mouse->sprite = 0;
      cheese->sprite = 0;
      for (int i = 0; i < nbTrap; i++)
        trap[i].sprite = 0;
      mouse->exist = 1;
      SDL_RenderClear(renderer);
      SDL_Delay(75);
      return 0;
    }
  }
  if (SDL_GetKeyboardState(NULL)[SDL_SCANCODE_DOWN] && mouse->exist == 0) {
    if (mouse->y < 435) {
      mouse->y = mouse->y + 30;
      mouse->sprite = 0;
      cheese->sprite = 0;
      for (int i = 0; i < nbTrap; i++)
        trap[i].sprite = 0;
      mouse->exist = 1;
      SDL_RenderClear(renderer);
      SDL_Delay(75);
      return 0;
    }
  }
  return 1;
}

int pointsCount(struct count *points, SDL_Renderer *renderer,
                struct item *mouse, struct item *cheese, struct item *trap,
                SDL_Color color, TTF_Font *font) {
  if (cheese->exist == 1 && mouse->x == cheese->x && mouse->y == cheese->y) {
    points->i = points->i + 1;
    cheese->exist = 0;
    for (int i = 0; i < nbTrap; i++)
      trap[i].exist = 0;
  }
  SDL_Surface *tmp = NULL;
  SDL_Texture *text = NULL;
  sprintf(points->c, "%d", points->i);
  tmp = TTF_RenderText_Solid(font, points->c, color);
  text = SDL_CreateTextureFromSurface(renderer, tmp);
  SDL_FreeSurface(tmp);
  if (NULL == text) {
    fprintf(stderr, "Error SDL_CreateTextureFromSurface : %s", SDL_GetError());
    return -1;
  }
  SDL_Rect dst = {570, 2, 0, 0};
  SDL_QueryTexture(text, NULL, NULL, &dst.w, &dst.h);
  SDL_RenderCopy(renderer, text, NULL, &dst);
  return 0;
}

int timeCount(struct time *clockTime, struct count *timer, struct item *mouse,
              struct item *cheese, struct item *trap, struct status *layout,
              SDL_Renderer *renderer, SDL_Color color, TTF_Font *font,
              SDL_Texture *gameLayout) {
  clockTime->current = SDL_GetTicks();
  if (clockTime->current - clockTime->previous >= 600) { // time in ms
    timer->i = timer->i - 1;
    clockTime->previous = clockTime->current;
    mouse->sprite = 0;
    cheese->sprite = 0;
    for (int i = 0; i < nbTrap; i++)
      trap[i].sprite = 0;
    layout->current = 0;
    SDL_RenderClear(renderer);
  }
  SDL_Surface *tmp = NULL;
  SDL_Texture *text = NULL;
  sprintf(timer->c, "%d", timer->i);
  tmp = TTF_RenderText_Solid(font, timer->c, color);
  text = SDL_CreateTextureFromSurface(renderer, tmp);
  SDL_FreeSurface(tmp);
  if (NULL == text) {
    fprintf(stderr, "Error SDL_CreateTextureFromSurface : %s", SDL_GetError());
    return -1;
  }
  SDL_Rect dst = {300, 2, 0, 0};
  SDL_QueryTexture(text, NULL, NULL, &dst.w, &dst.h);
  SDL_RenderCopy(renderer, text, NULL, &dst);
  return 0;
}

int printResults(struct count *points, SDL_Renderer *renderer, SDL_Color color,
                 TTF_Font *font) {
  SDL_Surface *tmp = NULL;
  SDL_Texture *text = NULL;
  sprintf(points->c, "%d", points->i);
  tmp = TTF_RenderText_Blended(font, points->c, color);
  text = SDL_CreateTextureFromSurface(renderer, tmp);
  SDL_FreeSurface(tmp);
  if (NULL == text) {
    fprintf(stderr, "Error SDL_CreateTextureFromSurface : %s", SDL_GetError());
    return -1;
  }
  SDL_Rect dst = {270, 190, 0, 0};
  SDL_QueryTexture(text, NULL, NULL, &dst.w, &dst.h);
  SDL_RenderCopy(renderer, text, NULL, &dst);

  tmp = TTF_RenderText_Blended(font, "Results", color);
  text = SDL_CreateTextureFromSurface(renderer, tmp);
  SDL_FreeSurface(tmp);
  if (NULL == text) {
    fprintf(stderr, "Error SDL_CreateTextureFromSurface : %s", SDL_GetError());
    return -1;
  }
  SDL_Rect dst_corner = {200, 80, 0, 0};
  SDL_QueryTexture(text, NULL, NULL, &dst_corner.w, &dst_corner.h);
  SDL_RenderCopy(renderer, text, NULL, &dst_corner);

  return 0;
}

int printText(const char textToPrint[], SDL_Renderer *renderer, SDL_Rect dst,
              TTF_Font *font, SDL_Color color) {
  SDL_Surface *tmp = TTF_RenderText_Blended(font, textToPrint, color);
  SDL_Texture *text = SDL_CreateTextureFromSurface(renderer, tmp);
  SDL_FreeSurface(tmp);
  if (NULL == text) {
    fprintf(stderr, "Error SDL_CreateTextureFromSurface : %s", SDL_GetError());
    return -1;
  }
  SDL_QueryTexture(text, NULL, NULL, &dst.w, &dst.h);
  SDL_RenderCopy(renderer, text, NULL, &dst);
  return 0;
}

int helpButton(int x, int y) {
  if (0 <= x && x <= 110 && 370 <= y && y <= 480)
    return 1;
  else
    return 0;
}

int playButton(int x, int y) {
  if (187 <= x && x <= 432 && 311 <= y && y <= 422)
    return 1;
  else
    return 0;
}

int pauseButton(int x, int y) {
  if (0 <= x && x <= 58 && 0 <= y && y <= 42)
    return 1;
  else
    return 0;
}

int pauseButton2(int x, int y) {
  if (0 <= x && x <= 120 && 380 <= y && y <= 475)
    return 1;
  else
    return 0;
}

int pauseButton3(int x, int y) {
  if (510 <= x && x <= 635 && 380 <= y && y <= 475)
    return 1;
  else
    return 0;
}

int restartButton(int x, int y) {
  if (130 <= x && x <= 220 && 380 <= y && y <= 475)
    return 1;
  else
    return 0;
}

int easyButton(int x, int y) {
  if (220 <= x && x <= 420 && 100 <= y && y <= 160)
    return 1;
  else
    return 0;
}

int hardButton(int x, int y) {
  if (220 <= x && x <= 420 && 250 <= y && y <= 310)
    return 1;
  else
    return 0;
}
// end functions
